module subsc.smart_electronic {
    requires javafx.controls;
    requires javafx.fxml;

    opens subsc.smart_electronic to javafx.fxml;
    exports subsc.smart_electronic;
}
